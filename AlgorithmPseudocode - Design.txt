Step 1. Get dungeon dimensions.
Step 2. Generate dungeon based on dimensions.
Step 3. Generate the initial room at the centre of the map, give it random width and height values.
Step 4. Pick a random wall of the initial room. In this case, pick random x and y coordinates until a wall is found.
Step 5. Determine the orientation of this block. This can be done by detecting +/- 1 on each axis.
Step 6. Assign a direction for the new feature to be built based on the orientation, e.g. north, east, south, west.
Step 7. Pick a random feature to build, e.g. another room or corridor. Use weighted random number generation
		to weight probability of certain features like corridors spawning.
Step 8. Generate the new feature dimensions.
Step 9. Scan from the x and y coordinate of the block found in Step 4. in the direction found in step 6. along the width
		or height based on the direction, if there is room, place the blocks. If not go back to Step 4. If there is room, add
		the new feature.
Step 10. Repeat Step 4 through to 9 until maximum features limit is reached.
Step 11. Display the dungeon.

Requirements:

DataTypes:
		TileType - Floor, Wall, Earth to begin with.
		Direction - North, East, South, West
		FeatureType - Room, Corridor, Stairs, Ladder, etc... Each must have an attached probability value.
		Coordinates - X, Y, Z Axis. Y is Optional.

Functions:
		GenerateRoom - Generates a randomly sized room TileType 2D array.
		GenerateCorridor - Generates a randomly sized corridor of TileType 2D array.
		AddFeature - Adds a feature to a TileType array, e.g. a room, corridor, door or the like.
		DecideWhatToBuild - Calculates what to build based on weighted values.
		FillWalls - iterates over the entire map addings walls.
