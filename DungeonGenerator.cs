using System;
using System.Diagnostics;

namespace DungeonAlgorithm
{
	public class DungeonGenerator
	{

		#region Private Members

		private int _roomsToBuild = 3;
		private int _passagesToBuild = 4;
		private int _wallWidth = 1;
		private int _minRoomWidth = 8;
		private int _maxRoomWidth = 15;
		private int _minRoomHeight = 8;
		private int _maxRoomHeight = 15;
		private int _dungeonWidth = 50;
		private int _dungeonHeight = 50;
		private int _minPassageWidth = 3;
		private int _maxPassageWidth = 5;
		private int _minPassageLength = 5;
		private int _maxPassageLength = 15;
		private Random rand = new Random ();

		#endregion

		#region Public Properties

		/// <summary>
		/// Gets or sets the maximum length of a passage.
		/// </summary>
		/// <value>The maximum length of a passage.</value>
		public int MaximumPassageLength {
			get {
				return _maxPassageLength;
			}
			set {
				if (_maxPassageLength != value) {
					_maxPassageLength = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the minimum length of a passage.
		/// </summary>
		/// <value>The minimum length of a passage.</value>
		public int MinimumPassageLength {
			get {
				return _minPassageLength;
			}
			set {
				if (_minPassageLength != value) {
					_minPassageLength = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the maximum width of a passage.
		/// </summary>
		/// <value>The maximum width of a passage.</value>
		public int MaximumPassageWidth {
			get {
				return _maxPassageWidth;
			}
			set {
				if (_maxPassageWidth != value) {
					_maxPassageWidth = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the minimum width of a passage.
		/// </summary>
		/// <value>The minimum width of a passage.</value>
		public int MinimumPassageWidth {
			get {
				return _minPassageWidth;
			}
			set {
				if (_minPassageWidth != value) {
					_minPassageWidth = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the width of the dungeon.
		/// </summary>
		/// <value>The width of the dungeon.</value>
		public int DungeonWidth {
			get {
				return _dungeonWidth; 
			}
			set {
				if (_dungeonWidth != value) {
					_dungeonWidth = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the height of the dungeon.
		/// </summary>
		/// <value>The height of the dungeon.</value>
		public int DungeonHeight {
			get {
				return _dungeonHeight; 
			}
			set {
				if (_dungeonHeight != value) {
					_dungeonHeight = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the width of walls in the dungeon.
		/// </summary>
		/// <value>The width of the wall.</value>
		public int WallWidth {
			get {
				return _wallWidth;
			}
			set { 
				_wallWidth = value;
			}
		}

		/// <summary>
		/// Gets or sets the minimum width of the room.
		/// </summary>
		/// <value>The minimum width of the room.</value>
		public int MinimumRoomWidth {
			get {
				return _minRoomWidth;
			}
			set {
				if (_minRoomWidth != value) {
					_minRoomWidth = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the maximum width of the room.
		/// </summary>
		/// <value>The maximum width of the room.</value>
		public int MaximumRoomWidth {
			get {
				return _maxRoomWidth; 
			}
			set {
				if (_maxRoomWidth != value) {
					_maxRoomWidth = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the minimum room height
		/// </summary>
		/// <value>The minimum height of the room.</value>
		public int MinimumRoomHeight {
			get {
				return _minRoomHeight;
			}
			set {
				if (_minRoomHeight != value) {
					_minRoomHeight = value;
				}
			}
		}

		/// <summary>
		/// Gets or sets the maximum room height.
		/// </summary>
		/// <value>The maximum height of the room.</value>
		public int MaximumRoomHeight {
			get {
				return _maxRoomHeight; 
			}
			set {
				_maxRoomHeight = value;
			}
		}

		/// <summary>
		/// Gets or sets the number of rooms to build.
		/// </summary>
		/// <value>The number of rooms to build.</value>
		public int RoomsToBuild {
			get {
				return _roomsToBuild;
			}
			set {
				_roomsToBuild = value;
			}
		}

		/// <summary>
		/// Gets or sets the number of passages to build.
		/// </summary>
		/// <value>The number of passages to build.</value>
		public int PassagesToBuild {
			get {
				return _passagesToBuild;
			}
			set {
				_passagesToBuild = value;
			}
		}

		#endregion

		#region Custom Datatypes

		public enum TileType
		{
			Earth,
			Wall,
			PassageFloor,
			PassageWall,
			Floor}
		;

		public enum Direction
		{
			North,
			East,
			South,
			West,
			Indeterminate}
		;

		public enum Feature
		{
			Room,
			Passage,
			Wall,
			Door}
		;

		public struct Vector
		{
			public Direction direction;
			public Coordinates coordinates;

			public Vector (Direction dir, Coordinates coords)
			{
				direction = dir;
				coordinates = coords;
			}
		}

		public struct Coordinates
		{
			public int x, y;

			public Coordinates (int xCoord, int yCoord)
			{
				x = xCoord;
				y = yCoord;
			}
		}

		public struct FeatureType
		{
			public FeatureType (Feature feat, double prob)
			{
				feature = feat;
				probability = prob;
			}

			public Feature feature;
			public double probability;
		}

		#endregion

		public void GenerateDungeon ()
		{
			//Stopwatch sw = new Stopwatch ();
			//sw.Start ();
			// Generate the dungeon tile map.
			TileType[,] dungeon = new TileType[DungeonWidth, DungeonHeight];

			int currentRoomCount = 0;
			int currentPassageCount = 0;

			// Generates a randomly sized room.
			TileType[,] firstRoom = GenerateRoom (MinimumRoomWidth, MaximumRoomWidth, MinimumRoomHeight, MaximumRoomHeight);
			// increment the room count by 1.
			currentRoomCount += 1;

			int firstRoomWidth = firstRoom.GetLength (0);
			int firstRoomHeight = firstRoom.GetLength (1);

			Console.WriteLine ("First room width: " + firstRoomWidth + " first room height " + firstRoomHeight);

			// set the first room coordinates to the centre of the dungeon.
			Coordinates firstRoomCoordinates = new Coordinates ((DungeonWidth - firstRoomWidth) / 2, (DungeonHeight - firstRoomHeight) / 2);

			// places the first room into the dungeon at the specified coordinates.
			dungeon = AddFeature (firstRoom, dungeon, firstRoomCoordinates);

			// creates an array of features and their probabilities to spawn.
			FeatureType[] features = new FeatureType[] {
				new FeatureType (Feature.Room, 0.50),
				new FeatureType (Feature.Passage, 0.50)
			};

			bool numFeaturesReached = false;
			while (!numFeaturesReached) {

				// Decides where to build the next feature
				Console.WriteLine ("Deciding where to build");
				Vector vect = GetAvailableVector (dungeon);
				Console.WriteLine ("Decided to build to the " + vect.direction + " at x:" + vect.coordinates.x + " y: " + vect.coordinates.y);
				// Decides what feature to build.
				Feature theNewFeatureToBuild = DecideWhatToBuild (features);
				Console.WriteLine ("Decided to build a " + theNewFeatureToBuild);

				TileType[,] newFeature = new TileType[,]{ };

				if (currentRoomCount < RoomsToBuild && theNewFeatureToBuild == Feature.Room) {
					Console.WriteLine ("Generating the " + theNewFeatureToBuild);
					newFeature = GenerateRoom (MinimumRoomWidth, MaximumRoomWidth, MinimumRoomHeight, MaximumRoomHeight);
					Console.WriteLine ("Checking to see if there is enough space available at specified coordinates.");
					if (IsSpace (newFeature, dungeon, vect)) {
						Console.WriteLine ("Tiles are vacant!");
						Console.WriteLine ("Adding new Feature");
						dungeon = AddFeature (newFeature, dungeon, vect.direction, vect.coordinates);
						dungeon = FillWalls (dungeon);
						Console.WriteLine ("Added new room.");
						currentRoomCount += 1;
						//features = new FeatureType[]{ new FeatureType (Feature.Room, 0.90), new FeatureType (Feature.Passage, 0.10) };
					} else {
						Console.WriteLine ("The new " + theNewFeatureToBuild + " does not have enough free space, aborting placement.");
					}
				} 

				if (currentPassageCount < PassagesToBuild && theNewFeatureToBuild == Feature.Passage) {
					Console.WriteLine ("Generating the " + theNewFeatureToBuild);
					newFeature = GeneratePassage (vect.direction);
					Console.WriteLine ("Checking to see if there is enough space available at specified coordinates.");
					if (IsSpace (newFeature, dungeon, vect)) {
						Console.WriteLine ("Tiles are vacant!");
						Console.WriteLine ("Adding new Feature");
						dungeon = AddFeature (newFeature, dungeon, vect.direction, vect.coordinates);
						dungeon = FillWalls (dungeon);
						Console.WriteLine ("Added new passage.");
						currentPassageCount += 1;
						//features = new FeatureType[]{ new FeatureType (Feature.Room, 0.90), new FeatureType (Feature.Passage, 0.10) };
						} else {
							Console.WriteLine ("The new " + theNewFeatureToBuild + " does not have enough free space, aborting placement."); 
						}
					}
				else if(currentPassageCount == PassagesToBuild && currentRoomCount == RoomsToBuild){
					numFeaturesReached = true;
				}
			} 

			dungeon = FillWalls (dungeon);
			PrintDungeon (dungeon);
		}

		public DungeonGenerator (int dungeonWidth, int dungeonHeight)
		{
			DungeonWidth = dungeonWidth;
			DungeonHeight = dungeonHeight;
		}

		/// <summary>
		/// Prints the dungeon.
		/// </summary>
		/// <param name="dungeon">Dungeon.</param>
		private static void PrintDungeon (TileType[,] dungeon)
		{

			int width = dungeon.GetLength (0);
			int height = dungeon.GetLength (1);

			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					if (dungeon [j, i] == TileType.Floor) {
						Console.ForegroundColor = ConsoleColor.Gray;
						Console.Write ("F");
					} else if (dungeon [j, i] == TileType.Earth) {
						Console.ForegroundColor = ConsoleColor.DarkGreen;
						Console.Write ("E");
					} else if (dungeon [j, i] == TileType.Wall) {
						Console.ForegroundColor = ConsoleColor.Black;
						Console.Write ("W");
					} else if (dungeon [j, i] == TileType.PassageFloor) {
						Console.ForegroundColor = ConsoleColor.DarkRed;
						Console.Write ("P");
					}
				}
				Console.Write (Environment.NewLine);
			}
			Console.ForegroundColor = ConsoleColor.Gray;
		}

		/// <summary>
		/// Generates a room.
		/// </summary>
		/// <returns>A 2D TileMap array with randomly generated width and height.</returns>
		/// <param name="roomXMin">Room X Axis minimum width.</param>
		/// <param name="roomXMax">Room X Axis maximum width.</param>
		/// <param name="roomYMin">Room Y Axis minimum height.</param>
		/// <param name="roomYMax">Room Y Axis maximum height.</param>
		private TileType[,] GenerateRoom (int roomXMin, int roomXMax, int roomYMin, int roomYMax)
		{
			// Generate random width value.
			int width = rand.Next (roomXMin, roomXMax);
			// Generate a random height value.
			int height = rand.Next (roomYMin, roomYMax);

			// Create a room tilemap.
			TileType[,] roomTileMap = new TileType[width, height];
			// start at first row, iterate across each column.
			for (int i = 0; i < height; i++) {
				for (int j = 0; j < width; j++) {
					roomTileMap [j, i] = TileType.Floor;
				}
			}
			return roomTileMap;
		}

		/// <summary>
		/// Generates a new passage.
		/// </summary>
		/// <returns>The new passage.</returns>
		/// <param name="direction">The orientation of the new passage</param>
		private TileType[,] GeneratePassage (Direction direction)
		{
			int passageWidth = rand.Next (MinimumPassageWidth, MaximumPassageWidth);
			int passageLength = rand.Next (MinimumPassageLength, MaximumPassageLength);

			TileType[,] passage = new TileType[,]{ };

			if (direction == Direction.North || direction == Direction.South) {
				passage = new TileType[passageWidth, passageLength];
				for (int i = 0; i < passageLength; i++) {
					for (int j = 0; j < passageWidth; j++) {
						passage [j, i] = TileType.PassageFloor;
					}
				}
			} else {
				passage = new TileType[passageLength, passageWidth];
				for (int i = 0; i < passageWidth; i++) {
					for (int j = 0; j < passageLength; j++) {
						passage [j, i] = TileType.PassageFloor;
					}
				}
			}
			return passage;
		}

		/// <summary>
		/// Adds the feature at the specified coordinates in the specified direction.
		/// </summary>
		/// <returns>A TileType 2D array that represents the dungeon with the feature added to it.</returns>
		/// <param name="feature">The Feature to add to the dungeon.</param>
		/// <param name="dungeon">The Dungeon to add the feature to.</param>
		/// <param name="direction">The Direction to add the feature in.</param>
		/// <param name="coordinates">The beginning Coordinates for the feature.</param>
		private TileType[,] AddFeature (TileType[,] feature, TileType[,] dungeon, Direction direction, Coordinates coordinates)
		{
			int featureWidth = feature.GetLength (0);
			int featureHeight = feature.GetLength (1);

			int startHeightCoord = coordinates.y;
			int startWidthCoord = coordinates.x;

			switch (direction) {
			case Direction.North:
				startHeightCoord = coordinates.y - featureHeight + 1;
				break;
			case Direction.East:
				break;
			case Direction.South:
				break;
			case Direction.West:
				startWidthCoord = coordinates.x - featureWidth + 1;
				break;
			}

			for (int i = startHeightCoord; i >= startHeightCoord && i < startHeightCoord + featureHeight && i < DungeonHeight; i++) {
				for (int j = startWidthCoord; j >= startWidthCoord && j < startWidthCoord + featureWidth && j < DungeonWidth; j++) {
					// Places the room at x,y coords.
					dungeon [j, i] = feature [j - startWidthCoord, i - startHeightCoord];
				}
			}

			return dungeon;
		}

		/// <summary>
		/// Adds the specified feature to the specified dungeon.
		/// </summary>
		/// <returns>The feature.</returns>
		/// <param name="feature">Feature.</param>
		/// <param name="dungeon">Dungeon.</param>
		/// <param name="coordinates">Coordinates.</param>
		private TileType[,] AddFeature (TileType[,] feature, TileType[,] dungeon, Coordinates coordinates)
		{
			int featureWidth = feature.GetLength (0);
			int featureHeight = feature.GetLength (1);

			// Iterate down the height axis
			for (int i = coordinates.y; i >= coordinates.y && i < coordinates.y + featureHeight && i < DungeonHeight; i++) {
				// iterate across the width axis.
				for (int j = coordinates.x; j >= coordinates.x && j < coordinates.x + featureWidth && j < DungeonWidth; j++) {
					// Places the room at x,y coords.
					dungeon [j, i] = feature [j - coordinates.x, i - coordinates.y];
				}
			}

			return dungeon;
		}

		/// <summary>
		/// Decides what to build based on the probabilities of each FeatureType in the provided FeatureType array.
		/// </summary>
		/// <returns>What to build.</returns>
		/// <param name="features">An array of FeatureTypes.</param>
		private Feature DecideWhatToBuild (FeatureType[] features)
		{
			double diceRoll = rand.NextDouble ();
			double runningTotal = 0;
			Feature theChosenFeature = new Feature ();
			for (int i = 0; i < features.Length; i++) {
				runningTotal += features [i].probability;
				if (diceRoll < runningTotal) {
					theChosenFeature = features [i].feature;
					break;
				}
			}
			return theChosenFeature;
		}

		private TileType[,] FillWalls (TileType[,] mapToFill)
		{
			int mapHeight = mapToFill.GetLength (1);
			int mapWidth = mapToFill.GetLength (0);
			for (int i = 0; i < mapHeight; i++) {
				for (int j = 0; j < mapWidth; j++) {
					// checks the current tile to see if there is not earth present
					if (mapToFill [j, i] != TileType.Earth) {
						// Checks to see if we're at the edge of the dungeon boundaries, if we are, then fill with walls.
						if (j == mapWidth - 1 || j == 0 || i == 0 || i == mapHeight - 1) {
							mapToFill [j, i] = TileType.Wall;
						}
						// checks to make sure that adding or subtracting one from the width index does not run out of bounds.
						if (j + 1 < mapWidth && j - 1 > 0) {
							// checks to see if the tile to the right of the current tile is earth or to the left, if it is, currently
							// selected tile is a wall. This covers both left and right walls.
							if (mapToFill [j + 1, i] == TileType.Earth || mapToFill [j - 1, i] == TileType.Earth) {
								mapToFill [j, i] = TileType.Wall;
							}
						}
						// checks to make sure adding or subtracting one from the height index does not run out of bounds.
						if (i + 1 < mapHeight && i - 1 > 0) {
							// checks to see if the tile above or below the currently selected tile is earth, it if is then the current tile
							// is a wall.
							if (mapToFill [j, i + 1] == TileType.Earth || mapToFill [j, i - 1] == TileType.Earth) {
								mapToFill [j, i] = TileType.Wall;
							}
						}
						// Checks to see if the tile below one and left one is earth, if it is the current tile is a wall.
						// this covers corner pieces facing into the dungeon
						if (j > 0 && i > 0 && j < mapWidth - 1 && i < mapHeight - 1) {
							if (mapToFill [j - 1, i + 1] == TileType.Earth ||
							    mapToFill [j + 1, i - 1] == TileType.Earth ||
							    mapToFill [j - 1, i - 1] == TileType.Earth ||
							    mapToFill [j + 1, i + 1] == TileType.Earth) {
								mapToFill [j, i] = TileType.Wall;
							}
						}
					}
				}
			}
			return mapToFill;
		}

		/// <summary>
		/// Gets a Vector that contains a direction and coordinates.
		/// </summary>
		/// <returns>The available vector.</returns>
		/// <param name="mapToScan">The TileMap to scan.</param>
		private Vector GetAvailableVector (TileType[,] mapToScan)
		{
			//TODO Set the X and Y positions properly in the return statements, compensate for this in the 
			// IsSpace function.
			int mapWidth = mapToScan.GetLength (0);
			int mapHeight = mapToScan.GetLength (1);

			bool canBuild = false;
			Coordinates coords = new Coordinates ();

			while (!canBuild) {

				coords = new Coordinates ();
				coords.x = rand.Next (mapWidth);
				coords.y = rand.Next (mapHeight);

				// means the random generated coords have found existing dungeon.
				if (mapToScan [coords.x, coords.y] != TileType.Earth) {
					// check to make sure we remain with the boundaries.
					if (coords.x - 1 > 0) {
						if (mapToScan [coords.x - 1, coords.y] == TileType.Earth) {
							coords.x = coords.x - 1;
							return new Vector (Direction.West, coords);
						}
					}

					if (coords.x + 1 < mapWidth) {
						if (mapToScan [coords.x + 1, coords.y] == TileType.Earth) {
							coords.x = coords.x + 1;
							return new Vector (Direction.East, coords);
						}
					}

					if (coords.y - 1 > 0) {
						if (mapToScan [coords.x, coords.y - 1] == TileType.Earth) {
							coords.y = coords.y - 1;
							return new Vector (Direction.North, coords);
						}
					}

					if (coords.y + 1 < mapWidth) {
						if (mapToScan [coords.x, coords.y + 1] == TileType.Earth) {
							coords.y = coords.y + 1;
							return new Vector (Direction.South, coords);
						}
					}
				}
			}

			return new Vector (Direction.Indeterminate, new Coordinates (0, 0));
		}

		/// <summary>
		/// Determines whether the feature can fit within the specified dungeon at the vectors position and direction.
		/// </summary>
		/// <returns><c>true</c> if this instance is vacant at the specified feature dungeon vect; otherwise, <c>false</c>.</returns>
		/// <param name="feature">Feature.</param>
		/// <param name="dungeon">Dungeon.</param>
		/// <param name="vect">Vect.</param>
		private bool IsSpace (TileType[,] feature, TileType[,] dungeon, Vector vect)
		{
			int featureWidth = feature.GetLength (0);
			int featureHeight = feature.GetLength (1);

			int startHeightCoord = vect.coordinates.y;
			int startWidthCoord = vect.coordinates.x;

			// checks to make sure that the new feature fits within the bounds of the dungeon.
			switch (vect.direction) {
			case Direction.North:
				if (vect.coordinates.y - featureHeight < 0) {
					return false;
				}
				if (vect.coordinates.x + MinimumPassageWidth-1 > DungeonWidth - 1) {
					return false;
				}
				if (dungeon [vect.coordinates.x + MinimumPassageWidth-1, vect.coordinates.y+1] == TileType.Earth) {
					return false;
				}
				vect.coordinates.y = vect.coordinates.y - featureHeight;
				break;

			case Direction.East:
				// check to make sure it can fit within the dungeon boundaries to the east.
				// TO-DO Check north/south as well.
				if (vect.coordinates.x + featureWidth > DungeonWidth - 1) {
					return false;
				}
				if (vect.coordinates.y + MinimumPassageWidth-1 > DungeonHeight - 1) {
					return false;
				}
					if (dungeon [vect.coordinates.x-1, vect.coordinates.y + MinimumPassageWidth-1] == TileType.Earth) {
					return false;
				}

				//vect.coordinates.x = vect.coordinates.x + WallWidth;
				break;

			case Direction.South:
				if (vect.coordinates.y + featureHeight > DungeonHeight - 1) {
					return false;
				}
				if (vect.coordinates.x + MinimumPassageWidth-1 > DungeonWidth - 1) {
					return false;
				}
				if (dungeon [vect.coordinates.x + MinimumPassageWidth-1, vect.coordinates.y-WallWidth] == TileType.Earth) {
					return false;
				}

				//vect.coordinates.y = vect.coordinates.y + featureHeight;
				break;

			case Direction.West:
				if (vect.coordinates.x - featureWidth < 0) {
					return false;
				}
				if (vect.coordinates.y + MinimumPassageWidth - 1 > DungeonHeight - 1) {
					return false;
				}
				if (dungeon [vect.coordinates.x+1, vect.coordinates.y + MinimumPassageWidth - 1] == TileType.Earth) {
					return false;
				}
				vect.coordinates.x = vect.coordinates.x - featureWidth;
				break;
			}

			// checks to make sure the new feature doesn't intersect an existing feature.
			for (int i = vect.coordinates.y; i >= vect.coordinates.y && i < vect.coordinates.y + featureHeight && i < DungeonHeight; i++) {
			for (int j = vect.coordinates.x; j >= vect.coordinates.x && j < vect.coordinates.x + featureWidth && j < DungeonWidth; j++) {
					// checks to see if there is anything obstructing the new feature placement.
					if (dungeon [j, i] != TileType.Earth) {
						return false;
					}
				}
			}
			return true;
		}
	}
}